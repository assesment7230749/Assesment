# Assessment Project

## Step 1: Set Up a Local Project

### Create a New Project:

Open a terminal or command prompt and navigate to the directory where you want to create your project. Run the following commands:

mkdir Assessment
cd Assessment
Initialize a Git Repository:
Run the following command to initialize a Git repository:

git init
Step 2: Create and Add Files
Create Project Files:
Create project files using your preferred IDE.

Add Files to Staging Area:
Run the following command to add the files to the staging area:

git add .
Commit Changes:
Commit the files to the local repository:

git commit -m "Initial commit"
Step 3: Set Up GitLab Repository
Create a GitLab Repository:

Log in to your GitLab account.
Click on the "+" icon in the upper right corner and select "New project."
Follow the prompts to create a new project.
Copy Repository URL:
After creating the GitLab repository, copy the repository URL provided by GitLab.

Step 4: Link Local Project to GitLab Repository
Add GitLab Repository as Remote:
In terminal, run the following command, replacing <repository_url> with the URL you copied from GitLab:

git remote add origin https://gitlab.com/assessment9020249/Assesment
Push to GitLab:
Push local repository to GitLab:

git push main
Provide GitLab credentials.

Step 5: Verify on GitLab
Visit GitLab Repository:
Open web browser and navigate to your GitLab repository to verify that project has been pushed successfully.

Consider creating and working on branches for features or bug fixes:

git checkout -b feature_branch(santosh)
Collaboration:


Step 7: Create feature file
Example :

Feature: Search for the product

Scenario: User searches for products successfully
When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
Then he sees the results displayed for "orange"
When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
Then he sees the results displayed for "Apple"
When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
Then he sees the results displayed for car "car"

Step 8: Create step-definition Search-product test
Create a Search-product Class:

Example:
package starter.step-definitions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenity.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
}

Step 9: Create TestRunner class

Exmple :

package starter.runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)



@CucumberOptions(  tags = "",features = {"src/test/resources/features/search/post_product.feature"}, glue = {"starter.stepdefinitions"},
plugin = { "pretty", "html:target/cucumber-reports" }, dryRun = true,monochrome = true)

public class TestRunner {}



Step 10: Push to GitLab Repository

Push Your Code to GitLab:
Commit your changes and push the code to your GitLab repository.

git add .
git commit -m "Add search product test cases"
git push origin master


Step 11: Monitor GitLab CI/CD Pipeline
Monitor Pipeline Execution:
Visit GitLab repository on the GitLab website and navigate to the "CI / CD > Pipelines" section to monitor the execution of pipeline.



Refactor :

The SerenityRest.given().when().get(endpoint); is used to make the GET request using the specified endpoint.
The SerenityRest.then().statusCode(200); is used to assert that the response status code is 200.
The SerenityRest.then().extract().body().asString().toLowerCase(); is used to extract and convert the response body to lowercase for case-insensitive comparison.
The SerenityRest.then().assertThat().body(Matchers.containsString(expectedProduct.toLowerCase())); is used to assert that the body contains the expected product.

Changes made:

The restAssuredThat method is statically imported to improve readability.
The import statement for the contains matcher is added.
The step definition method names are made more consistent.

Created TestRunner directory
Created TestRunner Class
