package starter.stepdefinitions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
        SerenityRest.then().log().all();

    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String expectedProduct) {
        restAssuredThat(response -> response.statusCode(200));
       // restAssuredThat(response -> response.body("requested_item", contains(expectedProduct.toLowerCase())));
    }

    @Then("he does not see any results")
    public void heDoesNotSeeAnyResults() {
        restAssuredThat(response -> response.statusCode(404));
        //restAssuredThat(response -> response.body("error", contains("true")));
    }

    @Then("he sees the results displayed for car {string}")
    public void heSeesTheResultsDisplayedForCar(String expectedProduct) {
        restAssuredThat(response -> response.statusCode(404));
      //  restAssuredThat(response -> response.body("requested_item", contains(expectedProduct.toLowerCase())));
    }
}

