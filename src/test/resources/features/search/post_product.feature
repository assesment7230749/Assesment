Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario: User searches for products successfully
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
    Then he sees the results displayed for "orange"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the results displayed for "Apple"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
    Then he sees the results displayed for car "car"

  Scenario: User searches for non-existent products
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/nonexistent/"
    Then he does not see any results

  Scenario: User searches for products with invalid characters
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/InvalidProduct%$#/"
    Then he does not see any results

  Scenario: User searches for products with empty search criteria
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/"
    Then he does not see any results

  Scenario: User searches for products with incomplete endpoint
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/"
    Then he does not see any results

