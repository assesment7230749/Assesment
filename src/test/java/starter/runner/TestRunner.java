package starter.runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)



@CucumberOptions(  tags = "",features = {"src/test/resources/features/search/post_product.feature"}, glue = {"starter.stepdefinitions"},
        plugin = { "pretty", "html:target/cucumber-reports" }, dryRun = true,monochrome = true)

public class TestRunner {}
